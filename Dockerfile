FROM ubuntu:18.04

ENV DEBIAN_FRONTEND "noninteractive"

ARG VERSION=foo

RUN mkdir /usr/src/nzbhydra2 /data
WORKDIR /usr/src/nzbhydra

RUN echo "About to get version: ${VERSION}"
ENV DOWNLOAD_URL=https://github.com/theotherp/nzbhydra2/releases/download/v${VERSION}/nzbhydra2-${VERSION}-linux.zip

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    unzip python3 wget python3-requests python3-yaml \
    gpg-agent software-properties-common && \
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ && \
    apt-get update && \
    apt-get install -y --no-install-recommends adoptopenjdk-13-openj9-jre && \
    wget -q ${DOWNLOAD_URL} && \
    unzip "nzbhydra2-${VERSION}-linux.zip" &&  \
    rm -f "nzbhydra2-${VERSION}-linux.zip" && \
    chmod 755 nzbhydra2 && \
    apt-get -y remove unzip wget software-properties-common gpg-agent && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/* && \
    useradd nzbhydra && \
    chown nzbhydra /data

USER nzbhydra

COPY configure-nzbhydra.py ./
COPY entrypoint.sh ./

CMD ["./entrypoint.sh"]
